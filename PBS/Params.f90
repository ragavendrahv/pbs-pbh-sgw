Module Params

IMPLICIT NONE

Integer, parameter :: n = 3, Ni = 0, Nf = 120
Complex*16, parameter :: II = DCMPLX(0,1)
Double precision, parameter :: PI = 4.0d0*atan(1.0d0)

Integer, parameter :: no_cores = 8

Logical, parameter :: USR1 = .FALSE.				! Model 1
Logical, parameter :: Bhaumik = .TRUE.			! Model 2
Logical, parameter :: critical_Higgs = .FALSE.		! Model 3
Logical, parameter :: USR2 = .FALSE.				! Model 4
Logical, parameter :: PI3 = .FALSE.				! Model 5
Logical, parameter :: string_inflation = .FALSE.		! Model 6

!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 12.0d0, e_i = 2.0d-3, phiN_i = -sqrt(2*e_i)			!RIPI
!Double precision, parameter :: m = 7.167d-8, phi_0 = 1.9654d0, Np = 90.0d0				!RIPI
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 16.0d0, e_i = 1.0d-2, phiN_i = -sqrt(2*e_i) 		!m2phi2-std.
!Double precision, parameter :: m = 7.0d-6, phi_0 = 1.0d0, Np = 50.0d0, kappa = 0.3d0		!m2phi2-std.
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 5.6d0, e_i = 1.0d-3, phiN_i = -sqrt(2*e_i)	!Staro
!Double precision, parameter :: m = (1.2d-9)**(0.25), phi_0 = sqrt(1.5d0)		!Staro
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 7.5d0, e_i = 1.0d-3, phiN_i = sqrt(2*e_i)	!small field
!Double precision, parameter :: m = (5.5d-10)**(0.25), phi_0 = 15.0d0			!small field

!Double precision, parameter :: phi_i = 7.5d0, e_i = 1.0d-3, phiN_i = sqrt(2*e_i)		!small field - for bump
!Double precision, parameter :: m = (5.5d-10)**(0.25), phi_0 = 15.0d0, Np = 50.0d0		!small field - for bump

!Double precision, parameter :: phi_i = 4.95d0, e_i = 2.99d0, phiN_i = sqrt(2*e_i)	!small field - KDI
!Double precision, parameter :: m = (3.6d-10)**(0.25), phi_0 = 15.0d0			!small field - KDI
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 13.5d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)			!AxM
!Double Precision, parameter ::  m = (1.6d-10)**(1.0d0/3.0d0), phi_0 = 7.6346d-3, kappa = 0.05d0	!AxM - DC
!Double Precision, parameter ::  m = (2.513d-10)**(1.0d0/3.0d0), phi_0 = 4.50299d-4, kappa = 0.05d0	!AxM - BINGO
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 0.849d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)	!Staro-II
!Double precision, parameter :: m = (2.37d-12)**(0.25), phi_0 = 0.707, kappa = 0.3d0	!Staro-II, Ref.: Martin, Sriram

!Double precision, parameter :: phi_i = 8.4348d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)	!Staro-II
!Double precision, parameter :: m = log10(2.98d1), phi_0 = 5.628d0, Np = 44.50d0		!Staro-II, against KDI
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 1.0d0, e_i = 1.0d0, phiN_i = sqrt(2*e_i)				!USR1 - for phase plot
!Double precision, parameter :: phi_i = 3.614d0, e_i = 1.0d-3, phiN_i = -sqrt(2.0*e_i)			!USR1 - Garcia Bellido - ref.: 1702.03901
!Double precision, parameter :: m = 4.0d-10, phi_0 = sqrt(0.108d0), Np = 5.0d1!, kappa = 3.0d-1		!USR1
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 2.0d0, e_i = 1.0d0, phiN_i = sqrt(2.0*e_i) 		! Bhaumik et al - for phase plot
Double precision, parameter :: phi_i = 17.245d0, e_i = 1.0d-2, phiN_i = -sqrt(2.0*e_i) 		! Bhaumik et al - used in the review
Double precision, parameter :: m = 1.8252976145649607E-009, phi_0 = 4.3411702837156332d0, Np = 55.0d0		! Bhaumik et al - used in the review
!!Double precision, parameter :: m = 1.3252976145649607E-009, phi_0 = 4.3411702837156332d0, Np = 58.0d0		! Bhaumik et al - original
!!Double precision, parameter :: m = 1.65E-009, phi_0 = 4.3411702837156332d0, Np = 55.0d0		! Bhaumik et al
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 8.0d0, e_i = 1.0d-3, phiN_i = -sqrt(2.0*e_i)			!critical Higgs
!Double precision, parameter :: m = 2.82d-8, phi_0 = 1.0d0, Np = 7.0d1!, kappa = 5.0d-2			!critical Higgs Ref. Higgs inflation - 1705.04861 - Eq.(12)
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 6.1d0, e_i = 1.0d-3, phiN_i = -sqrt(2.0*e_i)				!USR2 - Dalianis - Model II Table 2
!Double precision, parameter :: m = 2.0d-10, phi_0 = sqrt(6.0d0), Np = 5.0d1!, kappa = 3.0d-1		!USR2 - as in 1805.09483
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 7.4d0, e_i = 1.0d-3, phiN_i = -sqrt(2.0*e_i)			!PI3 - Dalianis - et.al. Fig.7 I_1
!Double precision, parameter :: m = 2.1d-10, phi_0 = sqrt(6.0d0), Np = 5.0d1!, kappa = 3.0d-1		!USR2 - as in 1805.09483
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 10.0d0, e_i = 2.0d-3, phiN_i = -sqrt(2.0*e_i)			! string inflation - Cicoli et al - JCAP 06 034 (2018)
!Double precision, parameter :: m = 3.4177d-10, phi_0 = 1.0d0, Np = 50.0d0					! string inflation - Cicoli et al - JCAP 06 034 (2018)
!----------------------------------------------------------------------------------------------------------------------------------------------

Double precision :: ai
Logical, parameter :: aiset = .FALSE.

Integer, parameter :: Nic_func = 1
!Nic_func = 1 => aH ; Nic_func = 2 => z"/z

Double precision, parameter :: Nic_depth = 5.0d2, Nshs_depth = 1.0d-7
Double precision, parameter :: kp = 5.0d-2, kmin = 1.0d-5, kmax = 1.0d20, k_test = 5.0d-2
Double precision, parameter :: k_kappa = 1.0d-1, ksqueez = 1.0d-5, k2D = kp, kappa = 0.3d0

! kpeak = 3.5d13 for Bhaumik et al
! kpeak = 1.8d13 for critical_Higgs
! kpeak = 3.8E11 for USR1

Integer, parameter :: NOS = 30000, Nadap = 1000, Nef = Nf-Ni				! NOS minimum 10^4 required for PPS;
Integer, parameter :: NOSD = 1000, NOS2D = NOSD!*NOSD	 			! NOS2D - pts. in 2D plane of k2/k1-k3/k1
Double precision, parameter :: toler = 1.0d-7

Double precision :: paramet1 = m, paramet2 = phi_0					! usually those to be constrained

Logical, parameter :: atype = .TRUE., btype = .FALSE.				! to choose b/w eps1i = 2.99 or 1.00

Logical, parameter :: include_step = .FALSE., include_bump = .FALSE., include_dip = .FALSE.

Logical, parameter ::  do_perturb = .TRUE., do_mode_test = .FALSE., do_RR = .FALSE., do_RRR = .TRUE., &
			& do_bulk = .TRUE., do_boundaries = .FALSE., &
			& do_kappa = .FALSE., do_equil = .TRUE., do_squeez = .FALSE., &
			& do_2D = .FALSE., do_3D = .FALSE.
!----------------------------------------------------------------------------------------------------------------------------------------------
Type BG
Integer :: NOOOS
Double Precision, dimension(:), allocatable :: efold, estep
Double Precision, dimension(:), allocatable :: phi, phiN
Double Precision, dimension(:), allocatable :: phiNN, phiNNN, a, zNN
Double Precision, dimension(:,:), allocatable :: Pot
Double Precision, dimension(:,:), allocatable :: H, z
Double Precision, dimension(2) :: paramets
End Type BG
!----------------------------------------------------------------------------------------------------------------------------------------------
Type Pert
Integer :: i_a, i_z, i_s
Double Precision :: ic_con, shs_con, star_con, Nic, Nshs, Nstar
Double Precision :: k, SPS, TPS, Rerr, Terr, SSRA
Complex*16, dimension(:), allocatable :: R, RN, Rgr, Rde
Complex*16, dimension(:), allocatable :: RC, RCN
Complex*16, dimension (:), allocatable :: T, TN
Complex*16, dimension(:), allocatable :: TC, TCN
Complex*16, dimension(:), allocatable :: S
End Type Pert
!----------------------------------------------------------------------------------------------------------------------------------------------
End Module Params
!----------------------------------------------------------------------------------------------------------------------------------------------
