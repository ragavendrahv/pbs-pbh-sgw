
set term pdf; set output "back_evol.pdf";

set xlabel "N"; set ylabel "{/Symbol f}";
plot "back_evol.txt" u 2:3 w l lw 2 title "{/Symbol f}(N)"
set xlabel "N"; set ylabel "log_{10} {/Symbol e}_1";
plot "back_evol.txt" u 2:(log10($4)) w l lw 2 title "{/Symbol e}_1(N)"
set xlabel "{/Symbol f}"; set ylabel "V({/Symbol f})";
plot "back_evol.txt" u 3:5 w l lw 2 title "V({/Symbol f})"
set xlabel "N"; set ylabel "H(N)";
plot "back_evol.txt" u 2:6 w l lw 2 title "H(N)"

set term pdf; set output "PPS.pdf";

set xlabel "log_{10} k"; set ylabel "log_{10} P_{s,t}(k)";
plot "PPS_output.txt" u 1:(log10($2)) w l lw 2 title "scalars", "" u 1:(log10($3)) w l lw 2 title "tensors"

set term pdf; set output "PBS.pdf";

set xlabel "log_{10} k"; set ylabel "log_{10} G(k)";
plot "PBS_output.txt" u (log10($1)):(log10(abs($2+$4))) w l lw 2 title "G1+G3", \
 "PBS_output.txt" u (log10($1)):(log10(abs($3))) w l lw 2 title "G2", \
 "PBS_output.txt" u (log10($1)):(log10(abs($5+$8-$9))) w l lw 2 title "G4+G7", \
 "PBS_output.txt" u (log10($1)):(log10(abs($6+$7))) w l lw 2 title "G5+G6", \
 "PBS_output.txt" u (log10($1)):(log10(abs($10-$11))) w l lw 2 title "G8", \
 "PBS_output.txt" u (log10($1)):(log10(abs($12-$13))) w l lw 2 title "G9"

set xlabel "log_{10} k"; set ylabel "f_{NL}(k)";
plot "PBS_output.txt" u (log10($1)):($15) w l lw 2 title ""

set term qt; replot
