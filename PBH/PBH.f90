!----------------------------------------------------------------------------------------------------------------------------------------------
!started on 6/2/2018
!check transfer function
! all length units are set as Mpc
! all mass units are in kg
!18/11/19
! including fnl in calc. of betapbh
!19/05/20
! expression for RtoM and fPBH included from Inomata, et.al., 1701.02544
! mass units changed to g
!28/07/20
!included expressions for R(M) and fPBH as per self derived notes and draft. 
!----------------------------------------------------------------------------------------------------------------------------------------------

PROGRAM PBH

Use Params

Implicit None

Integer :: NOK, NK3, ioint, i, j, jj, NK_readin
Double precision, dimension(i_max) :: k_readin, SPS_readin
Double Precision :: e, s, integrand1, integrand2, del_min, del_max, delk3, D1, R, kR
Double Precision :: varianR, sigmaR, Nre, M, S3, E3, Betapbh, Omegapbh, fpbh, delnm		
Double Precision :: kre, dsigmadM, dBetadM, l, yy, feat
Double Precision, dimension(2) :: WR, WR1, WR2, WR3
Double Precision :: t1, t2, t3, t4, t5, dummy
Double Precision :: varianR_an, Betapbh_an, fpbh_an
Double precision, dimension(i_max) :: k, Matpower, Pripower		! late-time matter power spectrum and primordial power spectrum
Double precision, dimension(i_max) :: k1, k2, k3, Bispectrum, fNL

Logical, parameter :: SPS_numerical = .TRUE., SPS_analytical = .FALSE.

call cpu_time(t1)

k=0.0d0
k1 = 0.0d0
k2 = 0.0d0
k3 = 0.0d0
Pripower = 0.0d0
Matpower = 0.0d0
Bispectrum = 0.0d0
fNL = 0.0d0

!----------------------------------------------------------------------------------------------------------------------------------------------
!reading in power spectrum
!----------------------------------------------------------------------------------------------------------------------------------------------
 Open(unit = 2, status="old", action="read", file="PPS_output.txt")

	j=1

	do 

	Read(2,*,iostat=ioint) k_readin(j), SPS_readin(j)
	if(ioint .eq. 0) then
		j=j+1
		continue

	elseif(ioint .eq. -1) then			! END OF FILE
		NK_readin = j-1
	print*
	print*, "No. of k's=",NK_readin			! No. of k's
		exit
	else 
		print*
		write(0,*) "error in reading power spectrum, iostat=", ioint
		STOP
	end if

	end do
 Close(2)
!----------------------------------------------------------------------------------------------------------------------------------------------

NOK = NK_readin

do i=1, NOK, 1

k(i) = k_readin(i)
Pripower(i) = SPS(k(i)) 

end do

!----------------------------------------------------------------------------------------------------------------------------------------------

 Open (unit = 1, action = "write", file = "PBH_output.txt")

! write(1,*) "# As = ", As
! write(1,*) "# ns = ", ns
! write(1,*) "# coupler = ", coupler
! write(1,*) "# kfeat = ", kfeat
! write(1,*) "# delk = ", delk
! write(1,*) "#"

 write(1,*) "# M/MSun,	 Betapbh, 	fpbh,		R,	varianR,	varianR_an,	Betapbh_an,	fpbh_an"

do j = 1, NM, 1

!--------------------------------
dummy = 0.0d0 + (50.0d0)*DBLE(j)/DBLE(NM)								! range of 50 orders from Mmin
M = Mmin*(10.0d0**dummy)

D1 = (4.72d-7)*sqrt(0.2d0/efform)*((gf/geq)**(1.0d0/12.0d0))						! ref.: Notes
R = D1*sqrt(M)

!--------------------------------

Matpower = 0.0d0
S3 = 0.0d0
WR = 0.0d0

varianR = 0.0d0
sigmaR = 0.0d0
dsigmadM = 0.0d0

Betapbh = 0.0d0
dBetadM = 0.0d0
Omegapbh = 0.0d0

integrand1 = 0.0d0
integrand2 = 0.0d0

!----------------------------------------------------------------------------------------------------------------------------------------------
 	s = log(10.0d0)*(log10(kmax) - log10(kmin))/DBLE(NOK)
	do i=1, NOK-1, 4
											! integrating spectra for variance
jj = 0
l = 0.0d0
t1 = 0.0d0
t2 = 0.0d0
t3 = 0.0d0
t4 = 0.0d0
t5 = 0.0d0
!--------------------------------------------------------------------------------
!	l = k(i)
!	kR = l*R
!	Matpower(i) = Transfunc(kR)*Pripower(i)						! simple Euler
!	WR(:) = WindowR(kR)
!	integrand1 = (Matpower(i)*WR(1)*WR(1))
!	varianR = varianR + integrand1*s						! integrand*dk where dk = k*d(logk)
!--------------------------------------------------------------------------------

	if (i .le. (NOK-4)) then
											! Boole's rule	
	l = k(i)
	kR = l*R
	Matpower(i) = Transfunc(kR)*Pripower(i)
	WR(:) = WindowR(kR)
	t1 = Matpower(i)*WR(1)*WR(1)

	jj = i+1
	l = k(jj)
	kR = l*R
	Matpower(jj) = Transfunc(kR)*Pripower(jj)
	WR(:) = WindowR(kR)
	t2 = Matpower(jj)*WR(1)*WR(1)

	jj = i+2
	l = k(jj)
	kR = l*R
	Matpower(jj) = Transfunc(kR)*Pripower(jj)
	WR(:) = WindowR(kR)
	t3 = Matpower(jj)*WR(1)*WR(1)

	jj = i+3
	l = k(jj)
	kR = l*R	
	Matpower(jj) = Transfunc(kR)*Pripower(jj)
	WR(:) = WindowR(kR)
	t4 = Matpower(jj)*WR(1)*WR(1)

	jj = i+4
	l = k(jj)
	kR = l*R
	Matpower(jj) = Transfunc(kR)*Pripower(jj)
	WR(:) = WindowR(kR)
	t5 = Matpower(jj)*WR(1)*WR(1)

	varianR = varianR + 2.0d0*s*(7.0d0*t1 + 32.d0*t2 + 12.0d0*t3 + 32.0d0*t4 + 7.0d0*t5)/45.0d0

	end if
!---------------------------------------------------------------------------------
	end do
!----------------------------------------------------------------------------------------------------------------------------------------------

	sigmaR = sqrt(varianR)

	del_min = del_c/(sqrt(2.0d0)*sigmaR)
	del_max = 1.0d0/(sqrt(2.0d0)*sigmaR)

!	print*, "M=", M, "varianR= ", varianR

!----------------------------------------------------------------------------------------------------------------------------------------------

 Call integrate(Probab, del_min, del_max, Betapbh)

!----------------------------------------------------------------------------------------------------------------------------------------------

kre = 1.0d0/R
do i=1, NOK, 1

e = Nstart + (Nend-Nstart)*DBLE(i)/DBLE(NOK)
dummy = Omegaevol(e,OmNR0,OmR0,OmL0)
dummy = sqrt(dummy)*dexp(e)

	if (kre .ge. dummy) then
		Nre = e
		exit
	else
		continue
	end if

end do
!----------------------------------------------------------------------------------------------------------------------------------------------

dummy = Omegaevol(Nre,OmNR0,OmR0,OmL0)



!fpbh = (Betapbh*6.84d7)*((efform/0.2d0)**(1.5d0))*((geq/gf)**(0.25d0))*sqrt(1/M)							! ref.: Notes
fpbh = (2.0d0**(0.25d0))*(efform**(1.5d0))*Betapbh* &							! M in solar masses, Meq and MSun in consistent units.
		& (OmegaNR0h2/OmegaC0h2)*((geq/gf)**(0.25d0))*sqrt(Meq/(MSun*M))							! ref.: Notes

!-------------------------------------------------------
!varianR_an = 8.0d0*As*/81.0d0										! far from feature
!varianR_an = 8.0d0*As*(1.0d0+coupler)/81.0d0								! around feature
													! for analytical estimate
!varianR_an = 8.0d0*As*(1.0d0 + coupler*dexp(-(log(R*kfeat)**2/(2.0d0*delk**2)))/ &
!			& sqrt(2.0d0*PI*delk*delk))/81.0d0						! extended. approx. ? check ?

!varianR_an = 8.0d0*As*(1.0d0 + 2.0d0*coupler*((kfeat*R)**4)*dexp(-(kfeat*R)**2))/(81.0d0)		! delta fn. approx.

!Betapbh_an = sqrt(varianR_an/(2.0d0*PI))*dexp(-del_c*del_c/(2.0d0*varianR_an))/del_c

!fpbh_an = (Betapbh_an/8.0d-25)*(0.12/OmegaNR0h2)*((efform/0.2d0)**(1.5d0)) &
!		& *((106.75d0/gf)**(0.25d0))*sqrt(1.0d0/M)						! ref.: Inomata, et.al.
!-------------------------------------------------------

write(1,*) M, Betapbh, fpbh, R, varianR, varianR_an, Betapbh_an, fpbh_an
!--------------------------------

end do
	print*
	print*, " Output written in PBH_output.txt"
	Close(1)

!----------------------------------------------------------------------------------------------------------------------------------------------
!functions 
!----------------------------------------------------------------------------------------------------------------------------------------------
Contains

Double precision Function SPS(k)

Integer :: i
Double precision :: k

if (SPS_numerical) then

	do i=1, NK_readin, 1

	if (k .le. k_readin(i)) then
	SPS = SPS_readin(i)
	exit
	end if

	end do

else if (SPS_analytical) then

	SPS = SPS_func(k)

else
	print*, "Specify SPS - numerical or analytical !"
	STOP
end if

End function SPS

!--------------------------------------------------------------------

Double precision Function SPS_func(k)

Double precision :: k, feat

feat = 0.0d0

!feat = coupler/(1.0d0 + ((k-k1)/(delta))**2)
!feat = coupler*dexp(-(log(k/k1)/delk)**2)		! lognormal
!feat = coupler/(1.0d0 + (log(k/k1)/delk)**2)

feat = coupler*dexp(-(log(k/kfeat)**2/(2.0d0*delk**2)))/sqrt(2.0d0*PI*delk*delk)		! lognormal and delta fn. approx

SPS_func = (As*(k/kp)**(ns-1.0d0))*(1.0d0 + feat)

!As = 1.0d0
!k1 = 1.0d0
!delk = 1.0d-2					! SPS of Shi Pi
!feat = log(k/k1)/(sqrt(2.0d0)*delk)

!SPS_func = (As/(sqrt(2.0d0*PI)*delk))*dexp(-feat**2)


!feat = 0.0d0
!feat = alpha/(1.0d0 + ((k(i)-kfeat)/(delta))**2)
!feat = coupler*dexp(-(log(k(i)/kfeat)/delk)**2)

!feat = coupler*dexp(-(log(k(i)/kfeat)**2/(2.0d0*delk**2)))/sqrt(2.0d0*PI*delk*delk)		! lognormal and delta fn. approx

!Pripower(i) = (As*(k(i)/kp)**(ns-1.0d0))*(1.0d0 + feat)

!	ns = 0.96d0
!	Pripower(i) = (2.1d-9)*(k(i)/kp)**(ns-1.0d0)					! standard scalar spectrum
!---------------------------------------------------------------------
!	Pripower(i) = Pripower(i)*(1.0d0 + coupler*dexp(-(k(i)/kfeat)*(k(i)/kfeat)) )	! gaussian - linear

!	Pripower(i) = Pripower(i)*(1.0d0 + coupler*dexp(-(log10(k(i)/kfeat))**2) )	! gaussian - log
!---------------------------------------------------------------------
!	Pripower(i) = Pripower(i)*(1.0d0 + coupler*dexp(-(log10(k(i)/kfeat)/1.0d-3)**2) )	! ~ delta
!---------------------------------------------------------------------
!	if((k(i) .ge. kfeat)) then				! step like spectrum
!	Pripower(i) = 1.0d-2
!	else
!		continue
!	end if
!---------------------------------------------------------------------

End Function SPS_func

!--------------------------------------------------------------------

Function Transfunc(x)
Double precision :: x, kR4, Transfunc

kR4 = x*x*x*x

Transfunc = 16.0d0*kR4/81.0d0

End Function Transfunc

!----------------------------------------------------------------------------------------------------------------------------------------------

Function WindowR(x)
Double precision :: x, arg
Double precision, dimension(2) :: WindowR

arg = -x*x/2.0d0

WindowR(1) = dexp(arg)								! ref. Peacock

WindowR(2) = D1*x*WindowR(1)/R						! derivative w.r.t. M

!x = k*R/sqrt(3.0d0)
!WindowR(1) = 3.*(sin(x)/x**3 - cos(x)/x**2)				! Liddle and Lyth
!WindowR(2) = (D2/sqrt(3.))*(3.*cos(x)/x**3 + (x**2-3.)*sin(x)/x**4)	
End function WindowR

!----------------------------------------------------------------------------------------------------------------------------------------------

Function Probab(delta)
Double precision :: delta, Probab					! here, delta = \nu = del/sigma

Probab = dexp(-delta*delta)/sqrt(PI)

End function Probab

!----------------------------------------------------------------------------------------------------------------------------------------------

Function Omegaevol(e, OmegaNR0, OmegaR0, OmegaL0)
Double precision :: e, Omegaevol
Double precision :: OmegaNR0, OmegaR0, OmegaL0

Omegaevol = OmegaNR0*dexp(-3.0d0*e) + OmegaR0*dexp(-4.0d0*e) + OmegaL0

End function Omegaevol

!----------------------------------------------------------------------------------------------------------------------------------------------

Recursive Function integral(integrand,ilower,iupper) Result (res)
Double precision :: res, integrand, ilower, iupper
Double precision :: integral0, integral1, integral2
Double precision :: step, eps, dummy, dummy2
Integer :: i

eps = 1.0d-15
step = (iupper-ilower)
res = 0.0d0

dummy = integrand(ilower)*step
dummy2 = ilower + step/2.0d0
integral0 = integrand(ilower)*step/2.0d0 + integrand(dummy2)*step/2.0d0

if(abs(integral0-dummy) .ge. eps) then

dummy2 = (ilower + step/2.0d0)
integral1 = integral(integrand, ilower, dummy2)
integral2 = integral(integrand, dummy2, iupper)
res = integral1 + integral2

else 
res = dummy

end if

End function integral

Subroutine integrate(integrand,ilower,iupper,res)
Double precision :: res, integrand, ilower, iupper
Double precision :: step, dummy, t1, t2, t3, t4, t5
Integer :: i, jj

Interface 
	Function integrand(dummy)
	Double precision :: dummy
	end function
end interface

step = (iupper-ilower)/DBLE(NOS)
res = 0.0d0

do i=1, NOS-1, 4

!--------------------------------------------------------------------------------

	!dummy = ilower + DBLE(i)*step								! trapezoidal
	!res = res + integrand(dummy)*step

!--------------------------------------------------------------------------------
	if (i .le. (NOS-4)) then
												! Boole's rule	
	dummy = ilower + DBLE(i)*step
	t1 = integrand(dummy)

	jj = i+1
	dummy = ilower + DBLE(jj)*step
	t2 = integrand(dummy)

	jj = i+2
	dummy = ilower + DBLE(jj)*step
	t3 = integrand(dummy)

	jj = i+3
	dummy = ilower + DBLE(jj)*step
	t4 = integrand(dummy)

	jj = i+4
	dummy = ilower + DBLE(jj)*step
	t5 = integrand(dummy)

	res = res + 2.0d0*step*(7.0d0*t1 + 3.2d1*t2 + 1.2d1*t3 + 3.2d1*t4 + 7.0d0*t5)/4.5d1

	end if
!---------------------------------------------------------------------------------

end do

End subroutine integrate
!----------------------------------------------------------------------------------------------------------------------------------------------
END PROGRAM PBH
!----------------------------------------------------------------------------------------------------------------------------------------------
