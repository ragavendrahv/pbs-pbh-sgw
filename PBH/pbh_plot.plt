
set term pdf; set output "PBH_output.pdf";
set autoscale
set format "%g"; set log xy;
set yrange [1e-30:]
set xlabel "M/M_{Sun}"; set ylabel "{/Symbol b}_{PBH}";
plot "PBH_output.txt" u 1:2 w l lw 2 title ""
set yrange [1e-15:]
set xlabel "M/M_{Sun}"; set ylabel "f_{PBH}";
plot "PBH_output.txt" u 1:3 w l lw 2 title ""
set autoscale
set xlabel "R/Mpc"; set ylabel "{/Symbol s^2}";
plot "PBH_output.txt" u 4:5 w l lw 2 title ""

set term qt; replot
