Module Params

IMPLICIT NONE

Integer, parameter :: i_max = 100000, NOS = 100000, NM = 1000
Double precision, parameter :: PI = 4.0d0*atan(1.0d0), Gravc  = 6.67d-11, &
				kp = 5.0d-2, keq = (1.022d-2), &
				C1 = 0.707, C2 = 0.3222, C3 = 0.3, &				! c1,c2,c2 = a,A,p as in Sheth-Tormen(check), Sirichai 
				efform = 0.2d0, &						! fraction of horizon mass going into PBH
				geq = 3.36d0, gf = 106.75d0, &					! eff. deg. of freedom at equality and formation time resp. Josan, Green, Malik
!				Meq = 6.196d50, MSun = 1.988d33					! Mass inside Hubble radius at equality in g
				Meq = 5.84d50, MSun = 1.988d33					! Mass inside Hubble radius at equality in g

Double precision, parameter ::  OmegaNR0h2 = 0.1424d0, OmegaC0h2 = 0.12d0, &			! fraction of total matter(baryons+CDM), fraction of CDM
				H0 = 67.3d0, OmNR0 = 0.314d0, Omega_CDM = 0.25d0, &
				OmR0 = 9.26d-5, &
				rho_c0 = 2.587d41, &						! critical density today in kg/Mpc^3
!				del_c = (1.0d0/3.0d0), &					! threshold of density contrast - original value, Carr et al
				del_c = 0.45d0, &						! threshold of density contrast - ref. arXiv: 0412063 and 1310.3007
				Mmin = 1.0d-28							! mass of interest, 10 orders less than the minimum = 10^15 g ~ 1.0d-18 MSun 
												! from Hawking radiation constraint

Double Precision, parameter :: As = dexp(3.089d0)*1.0d-10, ns = 0.96d0, &
				kfeat = 1.0d5, coupler = 8.0d7, &
				alpha = 1.0d7, delta = 1.0d8, delk = 1.5d0

Double precision, parameter :: OmL0 = (1.0d0 - OmNR0 - OmR0)
Double precision, parameter :: Nstart = -65.0d0, Nend = 0.0d0
Double precision, parameter :: kmin = 1.0d-5, kmax = 1.0d20					! smallest and largest mode of interest - in 1/Mpc

Logical :: nG = .FALSE.

End Module Params
