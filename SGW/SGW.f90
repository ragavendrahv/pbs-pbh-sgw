!_______________________________________________________________________________________________________!
!started on 6/5/20
!code to evolve tensor perturbations post inflation and calculate rel. energy density of GW at any given
!epoch
!as on 20/05/20
!major restructing
!adopted the expression of Espinosa, et.al. to evaluate \Omega_GW today
!_______________________________________________________________________________________________________!
!----------------------------------------------------------------------------------------------------------------------------------------------
Module Params

Implicit None

Type paramets
Integer :: Ninteg
Double precision :: llimit1, ulimit1
Double precision :: k, d
End type paramets

Type(paramets) :: p

Integer :: j, ioint, NK_readin
Integer, parameter :: no_cores = 8
Double precision, dimension(3000) :: k_readin, SPS_readin
Integer, parameter :: NOK = 1000, dNOS = 100, sNOS = 1000
Double precision, parameter :: PI = 4.0d0*atan(1.0d0), kmin = 1.0d-5, kmax = 1.0d20
Double precision, parameter :: As = dexp(3.089d0)*1.0d-10, ns = 1.0d0, kp = 5.0d-2
Double precision, parameter :: kfeat = 1.0d5, coupler = 2.0d7, delk = 1.0d0
Double precision, parameter :: dllimit = 0.0d0, dulimit = 1.0d0/sqrt(3.0d0), toler = 5.0d-2
Double precision, parameter :: sllimit = 1.0d0/sqrt(3.0d0), sulimit = 1.0d6
!Double precision, parameter :: Cg=0.4d0, Omega_R0=8.6d-5		old values
Double precision, parameter :: Cg=0.4d0, Omega_R0=4.16d-5		! updated value


Logical, parameter :: recurse = .FALSE.
Logical, parameter :: SPS_numerical = .TRUE., SPS_analytical = .FALSE.

interface
	double precision function func(x,p)
	import paramets	
	double precision :: x
	type(paramets) :: p
	end function func
end interface

procedure(func), pointer, public :: f1

End module Params

!----------------------------------------------------------------------------------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------------------------------------
PROGRAM SGW
!----------------------------------------------------------------------------------------------------------------------------------------------

Use Params
Use OMP_LIB

Implicit None

Integer :: i, chunk, num_threads, thrid
Double precision :: y, Integral2, sloc, dloc, prefac, dummy
Double precision :: modes(NOK), OmegaGW(NOK), OmegaGW_an(NOK)

f1 => Integrand1

!----------------------------------------------------------------------------------------------------------------------------------------------
!reading in power spectrum
!----------------------------------------------------------------------------------------------------------------------------------------------
 Open(unit = 2, status="old", action="read", file="PPS_output.txt")

	j=1

	do 

	Read(2,*,iostat=ioint) k_readin(j), SPS_readin(j)!, dummy
!				k_readin(j) = 10**(k_readin(j))
	if(ioint .eq. 0) then
		j=j+1
		continue

	elseif(ioint .eq. -1) then			! END OF FILE
		NK_readin = j-1
	print*
	print*, "No. of k's=",NK_readin			! No. of k's
		exit
	else 
		print*
		write(0,*) "error in reading power spectrum, iostat=", ioint
		STOP
	end if

	end do
 Close(2)
!----------------------------------------------------------------------------------------------------------------------------------------------

!$OMP PARALLEL NUM_THREADS(no_cores) DEFAULT(SHARED) &
!$OMP& PRIVATE(i,p,y,Integral2,thrid) &
!$OMP& SHARED(OmegaGW,modes) &
!$OMP& SHARED(chunk,num_threads)

thrid =  OMP_GET_THREAD_NUM()
num_threads = OMP_GET_NUM_THREADS()
chunk = NOK/num_threads/2

if (thrid .eq. 0) then
print*
print*, "number of threads = ", num_threads, " chunks = ", chunk
print*, "	k			OmegaGW				OmegaGW_an"
end if

p%llimit1 = log10(sllimit)
p%ulimit1 = log10(sulimit)

!$OMP DO SCHEDULE(DYNAMIC, chunk)
do i=1, NOK, 1

	y = log10(kmin) + DBLE(i)*(log10(kmax/kmin))/DBLE(NOK)
	modes(i) = (10.0d0)**y
	p%k = modes(i)

	p%Ninteg = dNOS
	Call Integrate(Integrand2, dllimit, dulimit, Integral2, p)

	OmegaGW(i) = (Cg*Omega_R0/3.6d1)*Integral2			! old expression

OmegaGW_an(i) = (Cg*Omega_R0/3.6d1)*As*As*coupler*coupler*OmegaGW_lognorm(modes(i), kfeat, delk) + 5.0d-23				!analytical estimate

if (thrid .eq. 0) then
	write(*,*) modes(i), OmegaGW(i), OmegaGW_an(i)
end if
end do

!$OMP END DO
!$OMP END PARALLEL

!--------------------------------------------------------------------
!output
!--------------------------------------------------------------------

Open(unit=1,file="SGW_output.txt")

write(1,*) "# upper limit of s = ", sulimit
write(1,*) "# Method of integration = boole's rule"
write(1,*) "# power spectrum params: "
write(1,*) "# As = ", As
write(1,*) "# ns = ", ns
write(1,*) "# kp = ", kp
write(1,*) "# feature params: Gaussian"
write(1,*) "# kfeat = ", kfeat
write(1,*) "# coupler = ",coupler
write(1,*) "# delk = ", delk
write(1,*) "#"
write(1,*) "#	k			OmegaGW				Ps"

do i=1, NOK, 1

	y = log10(kmin) + DBLE(i)*(log10(kmax/kmin))/DBLE(NOK)
	modes(i) = (10.0d0)**y

	write(1,*) modes(i), OmegaGW(i), SPS(modes(i)), OmegaGW_an(i)
end do
	print*
	print*, "output written in SGW_output.txt"
	Close(1)
!----------------------

!p%k = 1.0d8
!p%d = 1.0d0/3.0d0

!write(4,*) "#k =", p%k
!write(4,*) "#d =", p%d					testing upper limit of s integral
!write(4,*) "#"
!write(4,*) "#	s,	Integrand"

!NOK = 10000
!do i=1, NOK, 1

!	sloc = p%llimit1 + (p%ulimit1 - p%llimit1)*DBLE(i)/DBLE(NOK)
!	y = Integrand1(sloc, p)

!write(4,*) sloc, y

!end do
!----------------------
!--------------------------------------------------------------------

Contains

Double precision Function Theta(t)

Double precision :: t

If (t .gt. 0.0d0) then
	Theta = 1.0d0
Else
	Theta = 0.0d0
End if

End Function Theta
!--------------------------------------------------------------------

Double precision Function Ic(d, s)

Double precision :: s, d, thetarg

thetarg = s-1.0d0
Ic = -3.6d1*PI*Theta(thetarg)*((s*s + d*d - 2.0d0)**2)/((s*s - d*d)**3)

End Function Ic

!--------------------------------------------------------------------

Double precision Function Is(d, s)

Double precision :: s, d, dummy, c1, c2

 c1 = (s*s + d*d - 2.0d0)/((s*s - d*d)**2)

dummy = log((1.0d0-d*d)/(abs(s*s-1.0d0)))
 c2 = (s*s + d*d - 2.0d0)*dummy/(s*s - d*d) + 2.0d0

Is = -3.6d1*c1*c2

End Function Is

!--------------------------------------------------------------------

Double precision Function SPS(k)

Integer :: i
Double precision :: k

if (SPS_numerical) then

	do i=1, NK_readin, 1

	if (k .le. k_readin(i)) then
	SPS = SPS_readin(i)
	exit
	end if

	end do

else if (SPS_analytical) then

	SPS = SPS_func(k)

else
	print*, "Specify SPS - numerical or analytical !"
	STOP
end if

End function SPS

!--------------------------------------------------------------------

Double precision Function SPS_func(k)

Double precision :: k, delta, feat

feat = 0.0d0
!feat = coupler/(1.0d0 + ((k-k1)/(delta))**2)
!feat = coupler*dexp(-(log(k/k1)/delk)**2)		! lognormal
!feat = coupler/(1.0d0 + (log(k/k1)/delk)**2)

feat = coupler*dexp(-(log(k/kfeat)**2/(2.0d0*delk**2)))/sqrt(2.0d0*PI*delk*delk)		! lognormal and delta fn. approx

SPS_func = (As*(k/kp)**(ns-1.0d0))*(1.0d0 + feat)

!As = 1.0d0
!k1 = 1.0d0
!delk = 1.0d-2					! SPS of Shi Pi
!feat = log(k/k1)/(sqrt(2.0d0)*delk)

!SPS_func = (As/(sqrt(2.0d0*PI)*delk))*dexp(-feat**2)

End Function SPS_func

!--------------------------------------------------------------------

Double precision Function OmegaGW_lognorm(k, kfeat, delk)

Double precision :: k, kfeat, delk, kr, ke
Double precision :: t1, t2, t3, term1, term2, term3

kr = k/kfeat
ke = kr*dexp(3.0d0*delk*delk/2.0d0)

t1 = (log(ke)*log(ke) + delk*delk/2.0d0)*Erfc((log(ke) + 0.5d0*log(1.5d0))/delk)
t2 = delk * dexp(-(log(ke) + 0.5d0*log(1.5d0))**2/(delk*delk)) * (log(ke) - 0.5d0*log(1.5d0))/sqrt(PI)
term1 = (4.0d0*exp(9.0d0*delk**2/4.0d0)*kr**3)*(t1 - t2)/(5.0d0*sqrt(PI)*delk)

t1 = log(kr) + delk*delk + 0.5d0*log(4.0d0/3.0d0)
term2 = 6.59d-2*kr*kr*dexp(delk*delk)*dexp(-t1*t1/(delk*delk))/(delk*delk)

!term2 = term2 + 2.0*sqrt(term2)		! testing

t1 = sqrt(2.0d0)*dexp(8.0d0*delk*delk)/(3.0d0*sqrt(PI)*delk*kr**4)
t2 = dexp(-log(kr)*log(kr)/(2.0d0*delk*delk))
t3 = (4.0d0*delk*delk - log(kr/4.0d0))/(delk*sqrt(2.0d0))
term3 = t1*t2*Erfc(t3)

OmegaGW_lognorm = (term1 + term2 + term3)

End Function OmegaGW_lognorm

!--------------------------------------------------------------------

Double precision Function OmegaGW_delta(k)

Double precision :: k

OmegaGW_delta = As*As

End Function OmegaGW_delta

!--------------------------------------------------------------------

Double precision Function Integrand1 (s, p)

Double precision :: k, s, d, ps1, ps2, ic2, is2
Double precision :: prefac, arg_ps1, arg_ps2
Type (paramets) :: p

s = 10.0d0**s

k = p%k
d = p%d

prefac = (d*d - 1.0d0/3.0d0)*(s*s - 1.0d0/3.0d0)/(s*s - d*d)
prefac = prefac*prefac

arg_ps1 = sqrt(3.0d0)*k*(s+d)/2.0d0
arg_ps2 = sqrt(3.0d0)*k*(s-d)/2.0d0
ps1 = SPS(arg_ps1)
ps2 = SPS(arg_ps2)

ic2 = Ic(d,s)
ic2 = ic2*ic2
is2 = Is(d,s)
is2 = is2*is2

Integrand1 = prefac*ps1*ps2*(ic2 + is2)
Integrand1 = Integrand1*s

End function Integrand1

!--------------------------------------------------------------------

Double precision Function Integrand2 (d, p)

Double precision ::d, llimit1, ulimit1, Integral1
Type (paramets) :: p

llimit1 = p%llimit1
ulimit1 = p%ulimit1
p%d = d
p%Ninteg = sNOS

Call Integrate(Integrand1, llimit1, ulimit1, Integral1, p)
Integrand2 = Integral1

End function Integrand2

!--------------------------------------------------------------------

Recursive Subroutine Integrate(Integrand, llimit, ulimit, Integral, p)

Implicit None

Integer :: i, NOOS
Double precision :: llimit, ulimit, Integral, ierr
Double precision :: hlimit, Integral1, Integral2
Type (paramets) :: p

interface
double precision function Integrand(x,p)
import paramets
double precision :: x
Type (paramets) :: p
end function Integrand
end interface

Call Booles(Integrand, llimit, ulimit, Integral, p)

!--------recursion----------
if (recurse) then

hlimit = (llimit+ulimit)/2.0d0

Call Booles(Integrand, llimit, hlimit, Integral1, p)
Call Booles(Integrand, hlimit, ulimit, Integral2, p)

if ((Integral + Integral1 + Integral2) .ne. 0.0d0) then
	ierr = abs((Integral - Integral1 - Integral2)/(Integral + Integral1 + Integral2))
else 
!	print*
!	print*, "Integral = ", Integral
!	STOP

!	Integral = 1.0d-40
!	Integral1 = 1.0d-40
!	Integral2 = 1.0d-40

	ierr = 0.0d0
end if

if (ierr .gt. toler) then

	Call Integrate(Integrand, llimit, hlimit, Integral1, p)
	Call Integrate(Integrand, hlimit, ulimit, Integral2, p)

		Integral = Integral1 + Integral2

else if (ierr .le. toler) then

	Integral = Integral1 + Integral2
	return
else 
	print*
	print*, "Strange ierr"
	print*, Integral, Integral1, Integral2, ierr

	STOP
end if

end if
!--------recursion----------

End subroutine Integrate

!--------------------------------------------------------------------

Subroutine Booles(Integrand, llimit, ulimit, Integral, p)

Integer :: i, NOOS
Double precision :: llimit, ulimit, Integral
Double precision :: dx, x0, x1, x2, x3, x4, I0, I1, I2, I3, I4

Type (paramets) :: p

interface
double precision function Integrand(x,p)
import paramets
double precision :: x
Type (paramets) :: p
end function Integrand
end interface

NOOS = p%Ninteg

dx = (ulimit-llimit)/DBLE(NOOS)

Integral = 0.0d0
I0 = 0.0d0
I1 = 0.0d0
I2 = 0.0d0
I3 = 0.0d0 
I4 = 0.0d0

	do i=1, NOOS-4, 4

	x0 = llimit + DBLE(i)*dx
	x1 = llimit + DBLE(i+1)*dx
	x2 = llimit + DBLE(i+2)*dx
	x3 = llimit + DBLE(i+3)*dx
	x4 = llimit + DBLE(i+4)*dx

	I0 = Integrand(x0, p)
	I1 = Integrand(x1, p)
	I2 = Integrand(x2, p)
	I3 = Integrand(x3, p)
	I4 = Integrand(x4, p)

	Integral = Integral + (2.0d0*dx/45.0d0)*(7.0d0*I0 + 32.0d0*I1 + 12.0d0*I2 &
				& + 32.0d0*I3 + 7.0d0*I4)				! Ref. Boole's rule (Bode's rule in Abramowitz and Stegun)
	end do

End Subroutine Booles

!--------------------------------------------------------------------

Subroutine Simpsons(Integrand, llimit, ulimit, Integral, p)

Integer :: i, NOOS
Double precision :: llimit, ulimit, Integral
Double precision :: dx, x0, x1, x2, x3, x4, I0, I1, I2, I3, I4

Type (paramets) :: p

interface
double precision function Integrand(x,p)
import paramets
double precision :: x
Type (paramets) :: p
end function Integrand
end interface

NOOS = p%Ninteg

dx = (ulimit-llimit)/DBLE(NOOS)

Integral = 0.0d0
I0 = 0.0d0
I1 = 0.0d0
I2 = 0.0d0
I3 = 0.0d0 
I4 = 0.0d0

	do i=1, NOOS-4, 4

	x0 = llimit + DBLE(i)*dx
	x1 = llimit + DBLE(i+1)*dx
	x2 = llimit + DBLE(i+2)*dx
	x3 = llimit + DBLE(i+3)*dx
	x4 = llimit + DBLE(i+4)*dx

	I0 = Integrand(x0, p)
	I1 = Integrand(x1, p)								! Simpson's rule
	I2 = Integrand(x2, p)
	I3 = Integrand(x3, p)
	I4 = Integrand(x4, p)

!	Integral = Integral + dx*(I0 + 4.0d0*I1 + I2)/3.0d0
	Integral = Integral + dx*(I0 + 4.0d0*I1 + 2.0d0*I2 &
				& + 4.0d0*I3 + I4)/3.0d0
	end do

End Subroutine Simpsons
!--------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------------------------------------
END PROGRAM SGW
!----------------------------------------------------------------------------------------------------------------------------------------------
