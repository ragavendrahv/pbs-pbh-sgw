
set term pdf; set output "SGW_output.pdf";
set format "%g"; set log xy;
set yrange [1e-30:]
set xlabel "f/Hz"; set ylabel "{/Symbol W}_{GW}";
plot "SGW_output.txt" u 1:2 w l lw 2 title ""

set term qt; replot
