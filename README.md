# PBS-PBH-SGW

A Fortran package to compute scalar and tensor power spectra, scalar bispectrum, mass distribution of primordial black holes (PBH) and spectral density of
secondary gravitational waves (SGW) for a given model of canonical single field inflation

Project developed by H. V. Ragavendra

README under construction...

## Folders

PBS: to compute primordial power spectra of scalar and tensor perturbations and also compute the scalar bispectrum. 

PBH: compute the fraction of PBH composing current density of dark matter

SGW: compute the spectral density of secondary GWs.

## Models

The form of the potential and the values of the associated parameters for a given model can be specified in the files Params.f90 and HV.f90. Modify them as per the models of interest. Some interesting models have already been coded in. Comment/uncomment lines to choose the various models.

## Individual compilations and runs

The three parts in three sub-directories of the package can be compiled and run independently with suitable inputs.

The compilation and running can be done using the following commands within each sub-directory for respective outputs.
Compile using ``` make clean; make ```. Run using ```make run```. To obtain plots of outputs, use ```make plot```.

Running PBH and SGW requires the input of numerical scalar power spectrum titled PPS_output.txt. This can be obtained from previous instances of running PBS.

## Collective compilation and run

The entire setup can be run using the script file provided using the following command in the main directory.

```
bash get_ps_pbh_sgw
```

## Outputs

The numerical outputs are stored in text files with .txt extensions and the plots are generated in PDFs.
The outputs from individual runs are stored in the sub-directories whereas the outputs of collective run is stored in a separate sub-directory at ```./outputs/PBS/```.


## Authors and acknowledgment

Users of the package in part or whole can include reference to this project https://gitlab.com/ragavendrahv/pbs-pbh-sgw in their publications.
For queries and feedback, please write out to rhvhvr[AT]gmail.com

## License

To be added

## Project status

Undergoing minor edits to improve ease of user.
